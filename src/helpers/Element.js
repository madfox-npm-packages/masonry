
const DATASET = 'element';

const getElement = (name, parent = null) => {
    if (parent) {
        return parent.querySelector(`[data-${DATASET}="${name}"]`)
    } else {
        return document.querySelector(`[data-${DATASET}="${name}"]`)
    }
}

const getElements = (name, parent = null) => {
    if (parent) {
        return parent.querySelectorAll(`[data-${DATASET}="${name}"]`)
    } else {
        return document.querySelectorAll(`[data-${DATASET}="${name}"]`)
    }
}

export {
    getElement,
    getElements
}
