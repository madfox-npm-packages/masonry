/* eslint-disable */
import gulp from 'gulp';
import Browser from 'browser-sync';
import webpack from 'webpack';
import gulpNotify from 'gulp-notify';
import { stylesheets } from './styles';
import webpackDevMiddleware from 'webpack-dev-middleware';

import { config as webpackConfig } from './webpack';

export const browser = Browser.create();
const bundler = webpack(webpackConfig);

function reload(done) {
    browser.reload();
    done();
}

export const server = () => {

    const config = {
        server: {
            baseDir: "./example"
        },
        open: false,
        middleware: [
            webpackDevMiddleware(bundler, { /* options */ }),
        ]
    };

    browser.init(config);

    gulp.src('.')
        .pipe(gulpNotify('Server running on localhost'));

    gulp.watch('./src/**/*.js').on('change', () => browser.reload());
    gulp.watch('./src/scss/**/*.scss').on('change', () => stylesheets().pipe(browser.stream({match: '**/*.css'})));
};
