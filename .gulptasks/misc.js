/* eslint-disable */
import gulp from 'gulp';
import gulpClean from 'gulp-clean';

export const clean = () => gulp.src(['./example/js/build.js', './example/css/'], {read: false, allowEmpty: true})
    .pipe(gulpClean());

