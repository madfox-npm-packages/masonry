/* eslint-disable */
import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'autoprefixer';
import postcss from 'gulp-postcss';
import cleancss from 'gulp-clean-css';
import gulpif from 'gulp-if';

const isProduction = (process.env.NODE_ENV === 'production');

const paths = {
    src: './src/scss/*.scss',
    dest: './example/css/',
};

const config = {
    autoprefixer: {
        browsers: ['Explorer >= 10', 'iOs >= 7', 'last 3 versions'],
    },
    sass: {
        outputStyle: 'expanded',
        includePath: ['node_modules']
    },
}

const postCssPlugins = [
    autoprefixer(config.autoprefixer),
];

export const stylesheets = () => gulp.src(paths.src)
    .pipe(sass(config.sass))
    .on('error', sass.logError)
    .pipe(postcss(postCssPlugins))
    .on('error', () => {console.log('postcss failed')})
    .pipe(gulpif(isProduction, cleancss()))
    .on('error', () => {console.log('cleancss failed')} )
    .pipe(gulp.dest(paths.dest));
