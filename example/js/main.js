
const App = (() => {

    const init = () => {

    };

    return {
        init
    }
})();

if (document.readyState === 'loading') {
    window.addEventListener('DOMContentLoaded', App.init)
} else {
    App.init()
}
